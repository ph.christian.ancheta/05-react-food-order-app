# Food Order App - React App
A simple React App that allows users to order from a menu of food choices with respective price per item. Users can increase or decrease the quantity of food per food item. (Min 1, Max 5 per Food Item).

## Setup

```
npm install
npm run start
```
